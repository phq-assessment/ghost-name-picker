# Ghost Name Picker - Personal Project

The goal is to create a scalable web application to provide a randon ghost name when a user provides their own first and last name. All ghost names must be unique. The same ghost name should be consistently displayed to a returning user. This is application is hosted on Google Appengine.

## Project Preview

Need to have knowledge on Python - Flask and Google AppEngine for this app. Find some valuable resources on various online resources, which would help understand the concept to complete this task.


This python3 flask application for generating random **Ghost Name** for visiting users can be summarised as below:

- User needs to log in using Google authentication.
- New users need to put First and Last name.
- Emails associated to the users are acquired from the google account.
- Exisitng registered users can edit their ghost names.
- Shows a list of 3 possible Ghost Name results for the user to select from.
- Ghost Names follow the format [First Name] “[Ghost Name]” [Last Name].


## Requirements

1) Google App Engine

2) Python3.10

3) [Google Python3 secure scaffold](https://github.com/google/gae-secure-scaffold-python3#using-the-secure-scaffold-python-library  "Google Python3 secure scaffold")
4) Flask
5) Google Datastore
6) Python ndb
7) HTML/CSS/JS

## Installation

1) Create a folder '***workspace***' and open this folder in your editor (VS Code preffered). **Open the editor terminal**.

2) Go to your 'workpace' directory in the editor terminal and git pull the code using the command below:

***- % cd into the 'workspace' directory in the editor teminal before proceeding. You can copy n paste all the commands in this file.***

% cd workspace

```

git clone git@gitlab.com:phq-assessment/ghost-name-picker.git

```

3) Create virtualenv

```

python -m venv myenv

```

4) Enable virtualenv (*Please setup your own virtualenv before enabling*)

```

source myenv/bin/activate

```

5) Go inside the app main directory 'ghost-name-picker'.

```

cd ghost-name-picker/

```

6) Install the dependencies from requirements.txt

```

pip install -r requirements.txt

```

7) Optional, upgrade pip, if alered:

```

pip install --upgrade pip

```

8) Duplicate existing 'env.example' file and name it '.env'

```

cp env.example .env

```

9) Once .env file is created, provide the secure information as per the example below:

****Important Note**: During creation of OAuth 2.0 credentials for google login, you need to mention the 'Authorized redirect URIs' in the console. This redirect URL is the callback to our application after successfully logging in. 

Download the client_secret.json file and place in the main directory inside 'conection' folder. Update the 
redirect link in GOOGLE_SIGNIN_REDIRECT_URL as in the example below.


Hence predefined in my example.env file.

> GCLOUD_PROJECT=''
>
> GOOGLE_CLIENT_ID = ""
>
> GOOGLE_SECRET_KEY = ''
>
> GOOGLE_SIGNIN_REDIRECT_URL = 'http://host-name-here/callback'
>
> GOOGLE_CREDENTIALS_JSON_FILENAME = 'client_secret.json'
>
>
> #####Firebase - For Authenticaton
>
> APIKEY = ""
>
> PROJECT_ID = ""
>
>  <Include  other  variables  from  'env.example'  file>


10) Update '.env' file with all the required variables assigned.

11) Run flask app
  

## Author / Developer

Pavan Shashidharan

pavanshashidharan@gmail.com

+64 221668145