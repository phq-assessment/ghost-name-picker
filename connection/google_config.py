import os
import pathlib
from flask import request
from google_auth_oauthlib.flow import Flow
from dotenv import load_dotenv
# Load ENV
load_dotenv()

class GoogleConnection():
    
    google_client_id = os.environ["GOOGLE_CLIENT_ID"]
    
    def get_secrete_file(self):
        client_secrets_file = os.path.join(pathlib.Path(__file__).parent, os.environ["GOOGLE_CREDENTIALS_JSON_FILENAME"])
        return client_secrets_file

    def connect(self):
        flow = Flow.from_client_secrets_file(
            client_secrets_file=self.get_secrete_file(),
            scopes=["https://www.googleapis.com/auth/userinfo.profile", "https://www.googleapis.com/auth/userinfo.email", "openid"],
            redirect_uri=os.environ["GOOGLE_SIGNIN_REDIRECT_URL"]
        )
        return flow
