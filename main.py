import logging
import random
import securescaffold
import os
from dotenv import load_dotenv
from flask import request, render_template, redirect, send_from_directory, session, abort
from google.cloud import ndb
from models.users import User
from utils.ghost_setup import ghost_initial_set_up
from models.ghosts import Ghosts

# Google Autnetication
import requests
from google_auth_oauthlib.flow import Flow
from google.oauth2 import id_token
from pip._vendor import cachecontrol
import google.auth.transport.requests
import pathlib
from connection.google_config import GoogleConnection


# Load ENV
load_dotenv()
client = ndb.Client()
app = securescaffold.create_app(__name__, static_folder='static')

#Google Autnetication
app.secret_key = os.environ["GOOGLE_SECRET_KEY"]



def login_is_required(function):
    def wrapper(*args, **kwargs):
        if "google_id" not in session:
            return abort(401)  # Authorization required
        else:
            return function()

    return wrapper



@app.csrf.exempt
@app.route("/login/", methods=['GET', 'POST'])
def glogin():

    """
    This is the login for both GET and POST request for the application login with google.
    
    Method: POST:
        It is redirected to google authorization URL to be captured by the callback function in the app.
    
    """

    if request.method == 'POST':
        connect = GoogleConnection()
        flow=connect.connect()
        authorization_url, state = flow.authorization_url()
        session["state"] = state
        session["firstname"] = request.form.get('fname')
        session["lastname"] = request.form.get('lname')
        return redirect(authorization_url)
    else:
        if not('user' in session):
                return render_template('pages/login.html')
        else:
            abort(404)


@app.route("/callback")
def callback():

    """
    This is the callback function from google login. 

    We proceed to adding/updating the user details based on the 'Google Reference ID' caputured in this callback.

    """

    connect = GoogleConnection()
    flow=connect.connect()
    flow.fetch_token(authorization_response=request.url)
    credentials = flow.credentials
    request_session = requests.session()
    cached_session = cachecontrol.CacheControl(request_session)
    token_request = google.auth.transport.requests.Request(session=cached_session)

    id_info = id_token.verify_oauth2_token(
        id_token=credentials._id_token,
        request=token_request,
        audience=os.environ["GOOGLE_CLIENT_ID"]
    )

    # Store session details from google account
    session["google_id"] = id_info.get("sub")
    session["name"] = id_info.get("name")
    session["email"] = id_info.get("email")

    # User details to be saved in the database
    firstname = session["firstname"]
    lastname = session["lastname"]
    email = session["email"]
    google_id = session["google_id"]
    with client.context():
        user = User(first_name=firstname, last_name=lastname, email=email, google_id=google_id)
        new_user_key = ndb.Key(User, email)
        user.key = new_user_key
        user.put()
    return redirect("/")


@app.route("/logout/")
def glogout():

    """ Clear all the sessiom amd logout. """

    session.clear()
    return redirect("/")





@app.csrf.exempt
@app.route("/ghost_name_picker/pick-name", methods=['GET', 'POST'])
@login_is_required
def pickname():
        
        """ Name Picker login is handled in this section. """

        with client.context():
            current_user = session.get('google_id', None)
            if request.method == 'POST':
                if current_user:
                    assigned_ghost_name = request.form.get('assigned_ghost')
                    assigned_ghost = Ghosts.query(Ghosts.name==assigned_ghost_name).get()
                    current_user = session.get('current_user', None)
                    email = current_user['email']
                    user = User.query(User.email==email).get()

                    # get old ghost name to remove association
                    old_ghost = Ghosts.query(Ghosts.user==user.key).get()
                    if not old_ghost:
                        assigned_ghost.taken = True
                        assigned_ghost.user = user.key
                        assigned_ghost.put()
                        return redirect('/')  
                    else:
                        old_ghost.taken = False
                        old_ghost.user = None
                        # associate new ghost name
                        assigned_ghost.taken = True
                        assigned_ghost.user = user.key
                        ndb.put_multi([old_ghost, assigned_ghost])
                        return redirect('/')                   
            else:
                all_ghost_keys = Ghosts.query(Ghosts.taken==False).fetch(keys_only=True)
                selected_keys = random.sample(all_ghost_keys, 3)
                available_ghosts = []
                for selected_key in selected_keys:
                    available_ghosts.append(selected_key.get())

                user = User.query(User.google_id==session['google_id']).get()
                ghost_associated = Ghosts.query(Ghosts.user==user.key).get()

                if not ghost_associated:
                    header_text = 'Create your ghost name.'
                    ghost_name = None                    
                else:
                    header_text = 'Change your ghost name.'
                    ghost_name = ghost_associated.name
                current_user = {'first_name': user.first_name,
                    'last_name': user.last_name,
                    'email': user.email,
                    'ghost': ghost_name}
                session['current_user'] = current_user

        template_values = {
            'available_ghosts': available_ghosts,
            'current_user': current_user,
            'header_text': header_text
        }
        return render_template('name_picker.html', values=template_values)


 
@app.route("/")
def home():
 
    """ Loads the information required to display the list of ghost name to he user. """

    with client.context():
        ghost_instances = Ghosts.query().order(Ghosts.name).fetch()
        current_user = None
        if('google_id' in session):
            log_url_linktext = 'Logout'
            log_url= '/logout/'
            pick_linktext = 'Change your current Phantom name'
            user = User.query(User.google_id==session['google_id']).get()
            if user:
                associated_ghost = Ghosts.query(Ghosts.user==user.key).get()
                if associated_ghost:
                    current_user = {'first_name': user.first_name,
                                    'last_name': user.last_name,
                                    'email': user.email,
                                    'ghost': associated_ghost.name}
                    session['current_user'] = current_user
                else:
                    pick_linktext = 'Get a Phantom name'
                    current_user = {'first_name': user.first_name,
                                    'last_name': user.last_name,
                                    'email': user.email}
                    session['current_user'] = current_user

        else:
            log_url_linktext = 'Login'
            log_url= '/login/'
            pick_linktext = 'Get a Phantom name'            

        # gets list of ghosts
        ghosts = []        
        for ghost_instance in ghost_instances:
            user_email = ''
            full_name = ''            
            if ghost_instance.taken:
                ghost_user = ghost_instance.user.get()
                user_email = ghost_user.email
                full_name = ' '.join((ghost_user.first_name, ghost_user.last_name))

            ghosts.append({'name': ghost_instance.name, 'taken': ghost_instance.taken, 'user_email': user_email, 'user_full_name': full_name})                
        template_values = {
            'ghosts': ghosts,
            'current_user': current_user,
            'log_url': log_url,
            'log_url_linktext': log_url_linktext,
            'pick_linktext': pick_linktext
        }
    return render_template('overview.html', values=template_values)


@app.route("/reset", methods=['GET'])
def reset():

    """
    This is used to manually mark a 'Ghost Name' to available
    """

    with client.context():
        assigned_ghost_name = 'Bogle'
        assigned_ghost = Ghosts.query(Ghosts.name==assigned_ghost_name).get()
        assigned_ghost.taken = False
        assigned_ghost.user = None
        assigned_ghost.put()  
    session.clear()
    return redirect("/")


ghost_initial_set_up()


if __name__ == "__main__":
  app.run(port=os.environ['PORT'])