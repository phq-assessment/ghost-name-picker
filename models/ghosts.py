from google.cloud import ndb

from models.users import User


class Ghosts(ndb.Model):
    """
    All the Ghost name are updated in this Class
    """
    name = ndb.StringProperty(required=True)
    description = ndb.StringProperty()
    taken = ndb.BooleanProperty(default=False)
    user = ndb.KeyProperty(User)
