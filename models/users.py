from google.cloud import ndb


class User(ndb.Model):
    """
    Class that represents the users of the system.
    If they login with user account no record is saved.
    """
    first_name = ndb.StringProperty(required=True)
    last_name = ndb.StringProperty(required=True)
    email = ndb.StringProperty(required=False)
    google_id = ndb.StringProperty(required=True)