import os
import pathlib
from models.ghosts import Ghosts
import logging
import pandas as pd
from google.cloud import ndb
from dotenv import load_dotenv
load_dotenv()

client = ndb.Client()


def ghost_initial_set_up():
    
    """
    Runs on deploy.
    Populates datastore with the default set of ghosts if none exists.
    """

    with client.context():
        available_ghosts = Ghosts.query().fetch(keys_only=True, limit=10)
        if len(available_ghosts) == 0:
            logging.info('No ghosts available. Adding new ghosts...')

            # CSV file path to be imported
            filename = os.path.join(pathlib.Path(__file__).parent.parent, 'static/'+os.environ["NAMES_FILE"])

            # Process data to get it in a format we need to add to be added to the store
            data = pd.read_csv(filename)
            all_descrition = data['Description']
            all_descrition = all_descrition.to_dict()
            all_name = data['Ghost name']
            all_name = all_name.to_dict()

            all_name_sorted = {i:all_name[i] for i in all_descrition.keys()}

            keys = all_descrition.keys()
            values = zip(all_descrition.values(), all_name_sorted.values())
            dictionary = dict(zip(keys, values))

            ghost_to_add = []
            for each_row in dictionary:
                each_data = dictionary[each_row]
                gname = each_data[1]
                description = each_data[0]
                logging.info('Adding %s to datastore' % gname)
                description_validate = is_nan(description)
                ghost_to_add.append(Ghosts(name=gname, description=description_validate))
            
            ndb.put_multi(ghost_to_add)
            return True


def is_nan(x):

    """ If an value is 'Null' during the import, this will replace the field with a value. """

    if (x != x):
        return ''
    return x